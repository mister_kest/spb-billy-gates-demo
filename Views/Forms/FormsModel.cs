﻿using System;
using System.Collections.Generic;

namespace App.Views.Forms
{
    
    public class FormsModel
    {
        public IList<FormField> Fields { get; set; } = new List<FormField>();


        /// <summary>
        /// 
        /// </summary>
        public class FormField
        {
            public object Value { get; set; }
            public ICollection<Func<object, string>> Validators { get; set; } = new List<Func<object, string>>();
            public string Label { get; set; } = "Input";
            public string Icon { get; set; }
            public string Help { get; set; } = "This is a help";


            public string ElementType { get; set; } = "text";
            public string DataType { get; set; } = "System.String";
          

        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class FormBuilder
    {
        public FormsModel Form { get; set; } = new FormsModel() { 
            Fields = new List<FormsModel.FormField>()
            {
                new FormsModel.FormField(){
                    Label = "1"
                }
            }
        };

        public void AddTextField(string name)
        {
            var field = new FormsModel.FormField();
            field.Label = name;
            field.ElementType = "input-text";
            Form.Fields.Add(field);
        }
        public void AddMultiTextField(string name)
        {
            var field = new FormsModel.FormField();
            field.Label = name;
            field.ElementType = "input-multitext";
            Form.Fields.Add(field);
        }
        public void AddCheckboxField(string name)
        {
            var field = new FormsModel.FormField();
            field.Label = name;
            field.ElementType = "input-checkbox";
            Form.Fields.Add(field);
        }
        public void AddDateField(string name)
        {
            var field = new FormsModel.FormField();
            field.Label = name;
            field.ElementType = "input-date";
            Form.Fields.Add(field);
        }
        public void AddSelectField(string name)
        {
            var field = new FormsModel.FormField();
            field.Label = name;
            field.ElementType = "control-select";
            Form.Fields.Add(field);
        }
    }
}
