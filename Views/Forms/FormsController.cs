﻿using Microsoft.AspNetCore.Mvc;

namespace App.Views.Forms
{



    public class FormsController : Controller
    {
        private static FormBuilder Model = new FormBuilder();
        public IActionResult FormsView() => View(Model);
        public IActionResult AddTextField() { Model.AddTextField("Поле " + Model.Form.Fields.Count.ToString()); return RedirectToAction("FormsView"); }
        public IActionResult AddMultiTextField() { Model.AddMultiTextField("Поле " + Model.Form.Fields.Count.ToString()); return RedirectToAction("FormsView"); }
        public IActionResult AddDateField() { Model.AddDateField("Поле " + Model.Form.Fields.Count.ToString()); return RedirectToAction("FormsView"); }
        public IActionResult AddCheckboxField() { Model.AddCheckboxField("Поле " + Model.Form.Fields.Count.ToString()); return RedirectToAction("FormsView"); }
        public IActionResult AddSelectField() { Model.AddSelectField("Поле " + Model.Form.Fields.Count.ToString()); return RedirectToAction("FormsView"); }
        public IActionResult Index() => Redirect("/Forms/FormsView");
    }






    /// <summary>
    /// Базовая фформа ввода через форму HTML
    /// </summary>    
    public abstract class InputFormController<TInputModel>: Controller
    {

        public abstract TInputModel Create();




        public IActionResult OnGet()
        {
            return View(Create());
        }
        

        [HttpPost]
        public IActionResult OnPost(TInputModel Model)
        {
            if (ModelState.IsValid)
            {
                return OnSuccess(Model);
            }
            else
            {
                return View(Model);
            }            
        }



        public abstract IActionResult OnSuccess(TInputModel Model);

    }
}
